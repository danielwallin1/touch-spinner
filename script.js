;(function ($) {


$.fn.spin = function (options) {

	var data = $.extend({
		container    : $(this),
		colors       : ['#ffcb30','#69b0eb','#ff6262','#fcfc3c','#8ee555'], 
		width        : parseFloat($(window).width() * .82),
		height       : parseFloat($(window).width() * .82), 
		pieMargin    : 0, 
		memberstotal : [5, 7, 10]
	}, options);  

	var ctx             = undefined,  
		memberstotal    = data.memberstotal,
		datasum         = 0, 
		canvas          = undefined, 
		self            = this, 
		angle           = 0, 
		isDown          = false, 
		isSpinning      = false, 
		origPos         = 0, 
		rotation        = 1,  
		thresholdSwipe  = 0, 
		rotateDirection = 'none', 
		dx, 
		dy, 
		radians, 
		speed,
		timer,  
		counter;  

	this.init = function () {
		$.each(memberstotal, function () {
			datasum += this; 
		}); 

		this.createCanvas(); 
		this.createChart(); 

		data.container.swipe( {
		        swipeStatus:function(e, phase, direction, distance, duration, fingers)
		        {		        

		          if (phase == 'start') {
		          	isDown = true;
					mouseX = e.changedTouches[0].clientX;                   
					mouseY = e.changedTouches[0].clientY;

					dx = mouseX - (data.container.width() / 2); 
					dy = mouseY - (data.container.height() / 2); 
					radians = Math.atan2(dy, dx); 
					ry = radians * 180 / Math.PI - rotation;
					origPos = rotation;
					rotateDirection = "none";
		          }	

		          if (phase == 'move') {
						//if (isDown) {
							console.log("isDown ", isDown);
							mouseX = e.changedTouches[0].clientX;                   
							mouseY = e.changedTouches[0].clientY;
							dx = mouseX - (data.container.width() / 2); 
							dy = mouseY - (data.container.height() / 2); 
							radians = Math.atan2(dy, dx); 
							rx = radians * 180 / Math.PI;
							rotation = rx - ry;
							self.rotate(); 
							if (origPos < rotation) {
								rotateDirection = 'right'; 
								console.log(rotateDirection);
							} else if (origPos > rotation) {
								rotateDirection = 'left'; 
								console.log(rotateDirection);
							}		
						//}
				  }

				  if (phase == 'end' || phase == 'cancel') {
				  	//if (isDown) {
				  		isDown = false;

					  	if (distance >= thresholdSwipe) {
					  		console.log(duration);
					  		speed = Math.floor(Math.random() * 4 + 9);
							counter = 0;
							self.spin();
							self.disableSpinner(); 
					  	} else {
					  		console.log("You need to swipe more"); 
					  	}
				  	//}
				  }	

		        },
		        threshold:200,
		        maxTimeThreshold:5000,
		        fingers:'all'
	        }); 
        
	};  

	this.enableSpinner = function () {
		data.container.swipe("enable"); 
	}; 

	this.disableSpinner = function () {
		data.container.swipe("disable"); 
	}; 

	this.rotate = function () {
		data.container.css({ 
			"-moz-transform"    : "rotate(" + rotation + "deg)", 
			"-webkit-transform" : "rotate(" + rotation + "deg)", 
			"-o-transform"      : "rotate(" + rotation + "deg)" 
		});
	}; 

	this.spin = function () {

		if (rotation < 0) {
			console.log("rotation is negative");
		}
		if ( rotateDirection == 'right' || rotateDirection == 'up' || rotateDirection == 'downright' || rotateDirection == 'rightleft' ) {
			data.container.css({ 
				"-moz-transform"    : "rotate(" + rotation + "deg)", 
				"-webkit-transform" : "rotate(" + rotation + "deg)", 
				"-o-transform"      : "rotate(" + rotation + "deg)" 
			});
		} else if ( rotateDirection == 'left' || rotateDirection == 'down' || rotateDirection == 'downleft' || rotateDirection == 'leftright') {
			data.container.css({ 
				"-moz-transform"    : "rotate(-" + rotation + "deg)", 
				"-webkit-transform" : "rotate(-" + rotation + "deg)", 
				"-o-transform"      : "rotate(-" + rotation + "deg)" 
			}); 
		}

		counter++;
		
		if (counter % 50 == 0) {
			speed -= 2;
		}
		
		rotation += speed;

		if (rotation > 360) {
			rotation = rotation - 360;	
		}

		if (speed <= 0) {
			cancelAnimationFrame(timer);
			self.enableSpinner();  
		} else {
			timer = requestAnimationFrame(self.spin);
		}	

		
	}; 

	this.createCanvas = function () {
		var canvasNode = document.createElement("canvas"); 

		canvasNode.setAttribute( 'height', data.height );
		canvasNode.setAttribute( 'width', data.width );
		canvas = $(canvasNode);		

		var canvasContain = data.container.height(data.height).width(data.width).append(canvas);	

		ctx = canvas[0].getContext('2d');	
	}; 

	this.createChart = function () {
		var centerx = Math.round(canvas.width()/2), 
		    centery = Math.round(canvas.height()/2), 
		    radius = centery - data.pieMargin, 
		    counter = 0.0;       

			
		$.each(memberstotal, function(i) {

			var fraction = (this <= 0 || isNaN(this)) ? 0 : this / datasum;	
			console.log(5 / datasum); 		   
			ctx.beginPath();
			ctx.moveTo(centerx, centery);
			ctx.arc(centerx, centery, radius, 
				counter * Math.PI * 2 - Math.PI * 0.5,
				(counter + fraction) * Math.PI * 2 - Math.PI * 0.5,
	            false);
			var img = new Image(); 
			
	        ctx.lineTo(centerx, centery);
	        ctx.closePath();
/*	        img.addEventListener('load', function(e) {
	        	ctx.fillStyle = ctx.createPattern(img, 'repeat'); 
    			ctx.fill();
    		}, true);*/
	        ctx.fillStyle = data.colors[i];
	        ctx.fill();    
	        counter+=fraction;
	    });     
	}; 

	this.load = function (images) {
		for(img in images) {
			img.addEventListener('load', function(e) {
		        	ctx.fillStyle = ctx.createPattern(img, 'repeat'); 
	    			ctx.fill();
	    	}, true);
		}
		
	}; 

	this.setTransition = function () {
		data.container.css({ 
			"-webkit-transition" : "all 300ms ease-in-out",
			"-moz-transition"    : "all 300ms ease-in-out",
			"-o-transition "     : "all 300ms ease-in-out",
			"transition"         : "all 300ms ease-in-out",
		});	
	}; 	

	this.addMembers = function (data) {
		memberstotal = data; 	
	}; 

	return this; 
}

var spin = $('.pie').spin(); 
spin.addMembers([5, 5, 5, 10]);
spin.init(); 

})(jQuery); 